import diet.html;
import std.stdio;
import vibe.http.server;

void translateDiet() {
	auto file = File("public/index.html", "w");
	auto dst = file.lockingTextWriter;
	dst.compileHTMLDietFile!("index.dt", DefaultDietFilters);
	writeln("Generated index.html.");

	file = File("public/error.html", "w");
	dst = file.lockingTextWriter;
	dst.compileHTMLDietFile!("error.dt", DefaultDietFilters);
	writeln("Generated error.html.");
}
