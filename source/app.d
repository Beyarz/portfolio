import vibe.d : HTTPServerRequest, HTTPServerResponse, HTTPServerErrorInfo, render, URLRouter, serveStaticFiles, HTTPServerSettings, toDelegate, createTLSContext, TLSContextKind, listenHTTP, runApplication;
import std.format : format;
import std.file : append;

// Function to render index page
void index(scope HTTPServerRequest req, scope HTTPServerResponse res)
{
	res.headers["Content-Type"] = "text/html";
	res.render!("index.dt");
	append("logs/access.log", format!"Inbound: %s | Outbound: %s | %s"(req, res, req.clientAddress));
}

// Function to render error page
void error(scope HTTPServerRequest req, scope HTTPServerResponse res, scope HTTPServerErrorInfo err)
{
	res.headers["Content-Type"] = "text/html";
	res.render!("error.dt", req, err);
	append("logs/error.log", format!"Inbound: %s | Outbound: %s | %s | Error: %s"(req, res, req.clientAddress, err));
}

void main()
{
	// Router configuration
	auto router = new URLRouter;
	router.any("*", serveStaticFiles("public/"));
	router.get("/", &index);
	// router.any("/*", &error);

	// Read environment file
	import sdlang;
	Tag root = parseFile("env.sdl");
	int environmentPort = root.getTagAttribute!int("ip", "https");
	import std.conv : to;
	ushort envPort = to!ushort(environmentPort);
	string envIp = root.getTagValue!string("ip", "127.0.0.1");

	// Connection settings
	auto settings = new HTTPServerSettings;
	settings.port = envPort;
	settings.bindAddresses = [envIp];
	settings.errorPageHandler = toDelegate(&error);

	// Settings for TLS
	// settings.tlsContext = createTLSContext(TLSContextKind.server);
	// settings.tlsContext.useCertificateChainFile("certificate.crt");
	// settings.tlsContext.usePrivateKeyFile("private.key");

	version (convertDiet) {
		import strip;
		translateDiet();
	} else {
		// Start & run application
		listenHTTP(settings, router);
		import redirect;
		forward();
		runApplication();
	}
}
