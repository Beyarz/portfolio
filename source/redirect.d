import vibe.d : HTTPServerRequest, HTTPServerResponse, redirect, URLRouter, serveStaticFiles, HTTPServerSettings, listenHTTP;
import std.format : format;
import std.file : append;

// The handler for redirecting
void handler(scope HTTPServerRequest req, scope HTTPServerResponse res)
{
	res.headers["Content-Type"] = "text/html";
	res.redirect("https://name.domain/");
	append("redirect.log", format!"Inbound: %s | HTTP -> HTTPS | Outbound: %s | %s"(req, res, req.clientAddress));
}

/* Function to handle the inbounds on port
 * 80 and redirect it to port 443 for HTTPS
 */
void forward()
{
	// Router configuration
	auto router = new URLRouter;
	router.any("*", serveStaticFiles("public/"));
	router.any("/*", &handler);

	// Read environment file
	import sdlang;
	Tag root = parseFile("env.sdl");
	int environmentPort = root.getTagAttribute!int("ip", "http");
	import std.conv : to;
	ushort envPort = to!ushort(environmentPort);
	string envIp = root.getTagValue!string("ip", "127.0.0.1");

	// Connection settings
	auto settings = new HTTPServerSettings;
	settings.port = envPort;
	settings.bindAddresses = [envIp];

	// Start & run application
	listenHTTP(settings, router);
}
