# DMD compiler

It is highly recommended that you directly install DMD as it is the main compiler for the dlang.

`curl -fsS https://dlang.org/install.sh | bash -s dmd`

## Additional setup

```bash
sudo apt-get install libevent-dev libssl-dev
sudo apt-get install g++ gcc-multilib xdg-utils
snap install dub --classic
snap install dmd --classic
```

## Notes

You have to run dub inside the interactive compiler.

It should look something like this:
`(dmd-2.082.0)root@windows:/var/www/html/portfolio#`

## Step-by-step

### Starting with a fresh deployed Ubuntu 18

```bash
apt-get update
apt-get install apache2 --fix-missing
curl -fsS https://dlang.org/install.sh | bash -s dmd
source ~/dlang/dmd-2.082.0/activate
cd /var/www/html
rm index.html
git clone https://gitlab.com/Beyarz/portfolio.git
// Have to login
cd portfolio/
service apache2 stop
dub -v
sudo apt-get install libevent-dev libssl-dev
sudo apt-get install g++ gcc-multilib xdg-utils
sudo apt-get install libssl1.0-dev
dub
```

## Minimum requirements

- RAM: 4096 MB

## Issues

If you receive any errors about OpenSSL issue then downlod the following:
`sudo apt-get install libssl1.0-dev`

If you get any issues with GCC then you're probably not running inside the interactive compiler.
But an upgrade should resolve any outdated depency
`apt-get update`

If you get an error on the app.d file then that means an service is already running on port 80.
If you have apache/apache2 running then type the following:
You can check it by typing: `service --status-all`

Stop it by typing `service apache2 stop`
