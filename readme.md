# Compile & run

`dub`

## Build only

`dub build`

## Run only

`./portfolio`

## Convert Diet to Html

`dub --build=convertDiet`

## Dynamic variables

Variables such as port can be configured [here](env.sdl)

### Setup

See [setup.md](setup.md)
